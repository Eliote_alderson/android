package fall.fr.colorapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item2:
                Intent intentGray = new Intent(this,TograyActivity.class);
                startActivity(intentGray);
                Toast.makeText(this,"togray selected",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item3:
                Intent intentColorize = new Intent(this,ColorizeActivity.class);
                startActivity(intentColorize);
                Toast.makeText(this,"colorize selected",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item4:
                Toast.makeText(this,"keepcolor selected",Toast.LENGTH_SHORT).show();
                Intent intentKeepcolor = new Intent(this,KeepcolorActivity.class);
                startActivity(intentKeepcolor);
                return true;
            case R.id.subItem1:
                Toast.makeText(this,"contraste selected",Toast.LENGTH_SHORT).show();
                Intent intentContraste = new Intent(this,ContrasteActivity.class);
                startActivity(intentContraste);
                return true;
            case R.id.subItem2:
                Toast.makeText(this,"contraste selected",Toast.LENGTH_SHORT).show();
                Intent intentContrascolor = new Intent(this,ConstrasteColorActivity.class);
                startActivity(intentContrascolor);
                return true;
             default:
                 return super.onOptionsItemSelected(item);
        }
    }
}
