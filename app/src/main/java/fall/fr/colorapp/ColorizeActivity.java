package fall.fr.colorapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ColorizeActivity extends AppCompatActivity {

    Bitmap myImage_bm;
    int myImage_x, myImage_y;
    private Button demarrer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colorize);

        setTitle("COLORIZE");
        demarrer = (Button) findViewById(R.id.buttoncolorize);
        ImageView myImage = (ImageView) findViewById(R.id.imgcolorize);
        //myImageView.setImageResource(R.drawable.my_image);
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inMutable = true;
        myImage_bm = BitmapFactory.decodeResource(getResources(),R.drawable.football,o);

        myImage_y = myImage_bm.getHeight();
        myImage_x = myImage_bm.getWidth();

        Button colorizebutton = findViewById(R.id.buttoncolorize);
        colorizebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorize(myImage_bm);
            }
        });


        myImage.setImageBitmap(myImage_bm);

    }

    public void RGBToHSV_new(int red , int green , int blue , float[] h){
        float hh = 0 ;
        float r = (float) red / 255 ;
        float g = (float) green / 255 ;
        float b = (float) blue / 255 ;
        float cmax = Math.max(Math.max(r,g),b);
        float cmin = Math.min(Math.min(r,g),b);
        float diff = cmax - cmin ;

        // Calcule de H
        if (cmax == 0){
            h[0] = 0 ;
            h[1] = 0 ;
            h[2] = 0 ;
            return;
        }

        else if (cmax == r)
            hh = (g - b) / diff ;
        else if (cmax == g)
            hh = (r - g) / diff + 2 ;
        else if (cmax == b)
            hh = 4 + (r - g) / diff ;

        hh*=60.0 ;

        //Calcule de S
        if (hh < 0)
            hh+= 360 ;

        float s = diff / cmax ;

        h[0] = hh ;
        h[1] = s ;
        h[2] = cmax ;

    }

    public void colorize (Bitmap bmp){
        final int w = bmp.getWidth();
        final int h = bmp.getHeight();
        final int[] pixels = new int[w * h];
        int color;

        int pixR = 0;
        int pixG = 0;
        int pixB = 0;

        int pixColor = 0;
        float hsv[] =  new float[3];

        bmp.getPixels (pixels, 0, w, 0, 0, w, h);
        int pos = 0;


        for (int i = 0 ; i < h*w ; i++){

            pixColor = pixels[i];

            pixR = Color.red(pixColor);
            pixG = Color.green(pixColor);
            pixB = Color.blue(pixColor);

            RGBToHSV_new(pixR,pixG,pixB,hsv);

            int random = (int)( Math.random()*( 360 + 1 ) );
            float rand = (float) random;
            hsv[0] = (float)rand;  /*-->  est Hue \ ([0..360 [\)[0..360[*/
            //hsv[1] = 0; /*--> est la saturation \ ([0 ... 1] \)[0..1]*/
            //hsv[2] = 0; /*est la valeur \ ([0 ... 1] \)[0..1]*/


            // gris = (pixR + pixG + pixB) / 3;

            pixels[i] = Color.HSVToColor(hsv);
        }


        bmp.setPixels(pixels, 0, w, 0, 0, w, h);
    }



    /*--------------------------------------MENU ITEMS----------------------------------------------*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item2:
                Intent intentGray = new Intent(this,TograyActivity.class);
                startActivity(intentGray);
                Toast.makeText(this,"togray selected",Toast.LENGTH_LONG).show();
                return true;
            case R.id.item3:
                Intent intentColorize = new Intent(this,ColorizeActivity.class);
                startActivity(intentColorize);
                Toast.makeText(this,"colorize selected",Toast.LENGTH_LONG).show();
                return true;
            case R.id.item4:
                Toast.makeText(this,"keepcolor selected",Toast.LENGTH_LONG).show();
                Intent intentKeepcolor = new Intent(this,KeepcolorActivity.class);
                startActivity(intentKeepcolor);
                return true;
            case R.id.subItem1:
                Toast.makeText(this,"contraste selected",Toast.LENGTH_LONG).show();
                Intent intentContraste = new Intent(this,ContrasteActivity.class);
                startActivity(intentContraste);
                return true;
            case R.id.subItem2:
                Toast.makeText(this,"contraste selected",Toast.LENGTH_LONG).show();
                Intent intentContrascolor = new Intent(this,ConstrasteColorActivity.class);
                startActivity(intentContrascolor);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    /*---------------------------------------FIN MENU-----------------------------------------------*/

}
// minsdk version 17)+


