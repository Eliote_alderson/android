package fall.fr.colorapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ContrasteActivity extends AppCompatActivity {

    Bitmap myImage_bm;
    int myImage_x, myImage_y;
    private Button demarrer, diminuer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contraste);

        setTitle("CONTRASTE");
        demarrer = (Button) findViewById(R.id.buttoncontraste);
        diminuer = (Button) findViewById(R.id.buttoncontrastemoins);
        ImageView myImage = (ImageView) findViewById(R.id.imgcontraste);
        //myImageView.setImageResource(R.drawable.my_image);
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inMutable = true;
        myImage_bm = BitmapFactory.decodeResource(getResources(),R.drawable.girlgray,o);

        myImage_y = myImage_bm.getHeight();
        myImage_x = myImage_bm.getWidth();


        demarrer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upContrasteLUT(myImage_bm);
            }
        });
        diminuer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lowContrasteLUT(myImage_bm);
            }
        });


        myImage.setImageBitmap(myImage_bm);
    }

    public int[] histogramme(Bitmap bmp, int c) {
        final int w = bmp.getWidth();
        final int h = bmp.getHeight();
        final int[] pixels = new int[w * h];
        int histo[]  = new int[256];


        int pixR = 0;
        //int pixG = 0;
        //int pixB = 0;

        int pixColor = 0;


        bmp.getPixels (pixels, 0, w, 0, 0, w, h);

        for (int i = 0 ; i < h*w ; i++){

            pixColor = pixels[i];

            if ( c == Color.RED){
                pixR = Color.red(pixColor);
            }
            if ( c == Color.GREEN){
                pixR = Color.green(pixColor);
            }
            if ( c == Color.BLUE){
                pixR = Color.blue(pixColor);
            }
            else{
                pixR = ( Color.red(pixColor) + Color.green(pixColor) + Color.blue(pixColor) ) / 3;

            }
            histo[pixR] ++;
        }
        return histo;
    }

    public int[] minMax(int[] histotab) {
        int tab[]  = new int[2];

        int min=0,max=0;

        for (int i = 0 ; i < 256 ; i++){
            if (histotab[i]!=0){
                min = histotab[i];
                break;
            }
        }
        for (int i = 255 ; i >=0 ; i--){
            if (histotab[i]!=0){
                max = histotab[i];
                break;
            }
        }
        tab[0]=min;
        tab[1]=max;
        return tab;
    }

    public void upContraste(Bitmap bmp){
        final int w = bmp.getWidth();
        final int h = bmp.getHeight();
        final int[] pixels = new int[w * h];
        int[] histo = histogramme(bmp, Color.RED);
        int[] minMax = minMax(histo);
        int color=0;

        int pixR = 0;
        //int pixG = 0;
        //int pixB = 0;

        bmp.getPixels (pixels, 0, w, 0, 0, w, h);

        int pixColor=0;
        for (int i = 0 ; i < h*w ; i++){
            pixColor = pixels[i];
            pixR = Color.red(pixColor);
            color = (255*(pixR-minMax[0]))/(minMax[1]-minMax[0]);
            pixels[i] = Color.rgb(color, color, color);
        }
        bmp.setPixels(pixels, 0, w, 0, 0, w, h);
    }


    /*--------------------Utilisation d'une Look Up Table------------------------------------*/
    public void upContrasteLUT(Bitmap bmp){
        final int w = bmp.getWidth();
        final int h = bmp.getHeight();
        final int[] pixels = new int[w * h];
        int[] histo = histogramme(bmp, Color.RED);
        int[] minMax = minMax(histo);
        int color=0;

        int pixR = 0;
        //int pixG = 0;
        //int pixB = 0;

        bmp.getPixels (pixels, 0, w, 0, 0, w, h);

        int pixColor=0;

        // Initialisation de la LUT
        int[] LUT = new  int[256];

        for(int ng = 0; ng < 256; ng++){
            LUT[ng] = (255*(ng-minMax[0]))/(minMax[1]-minMax[0]);
        }

        // Calcul de la transformation
        for (int i = 0 ; i < h*w ; i++){
            pixColor = pixels[i];
            pixR = Color.red(pixColor);
            color = LUT[pixR];
            pixels[i] = Color.rgb(color, color, color);
        }
        bmp.setPixels(pixels, 0, w, 0, 0, w, h);
    }

    public void lowContrasteLUT(Bitmap bmp){
        final int w = bmp.getWidth();
        final int h = bmp.getHeight();
        final int[] pixels = new int[w * h];
        int[] histo = histogramme(bmp, Color.RED);
        int[] minMax = minMax(histo);
        int dist = minMax[1] - minMax[0];
        int percent = (dist*10)/100;
        minMax[0] = minMax[0] + percent;
        minMax[1] = minMax[1] - percent;
        int color=0;

        int pixR = 0;
        //int pixG = 0;
        //int pixB = 0;

        bmp.getPixels (pixels, 0, w, 0, 0, w, h);

        int pixColor=0;

        // Initialisation de la LUT
        int[] LUT = new  int[256];

        for(int ng = 0; ng < 256; ng++){
            LUT[ng] = ((ng*(minMax[1]-minMax[0]))/255)+minMax[0];
        }

        // Calcul de la transformation
        for (int i = 0 ; i < h*w ; i++){
            pixColor = pixels[i];
            pixR = Color.red(pixColor);
            color = LUT[pixR];
            pixels[i] = Color.rgb(color, color, color);
        }
        bmp.setPixels(pixels, 0, w, 0, 0, w, h);
    }


    /*--------------------------------------MENU ITEMS----------------------------------------------*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item2:
                Intent intentGray = new Intent(this,TograyActivity.class);
                startActivity(intentGray);
                Toast.makeText(this,"togray selected",Toast.LENGTH_LONG).show();
                return true;
            case R.id.item3:
                Intent intentColorize = new Intent(this,ColorizeActivity.class);
                startActivity(intentColorize);
                Toast.makeText(this,"colorize selected",Toast.LENGTH_LONG).show();
                return true;
            case R.id.item4:
                Toast.makeText(this,"keepcolor selected",Toast.LENGTH_LONG).show();
                Intent intentKeepcolor = new Intent(this,KeepcolorActivity.class);
                startActivity(intentKeepcolor);
                return true;
            case R.id.subItem1:
                Toast.makeText(this,"contraste selected",Toast.LENGTH_LONG).show();
                Intent intentContraste = new Intent(this,ContrasteActivity.class);
                startActivity(intentContraste);
                return true;
            case R.id.subItem2:
                Toast.makeText(this,"contraste selected",Toast.LENGTH_LONG).show();
                Intent intentContrascolor = new Intent(this,ConstrasteColorActivity.class);
                startActivity(intentContrascolor);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    /*---------------------------------------FIN MENU-----------------------------------------------*/


}
